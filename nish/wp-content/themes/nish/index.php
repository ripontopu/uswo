<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Nish</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/normalize.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/animate.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url');?>" />
        <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        
		
		<div class="header_area">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="logo">
							<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="" /></a>
						</div>
					</div>
					<div class="col-md-8">
						<div class="main_menu">
							<ul>
								<li><a href="#">Home</a></li>
								<li><a href="#">Home</a></li>
								<li><a href="#">Home</a></li>
								<li><a href="#">Home</a></li>
								<li><a href="#">Home</a></li>
								<li><a href="#">Home</a></li>
								<li><a href="#">Home</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="slider_area">
			<div class="container">
				<div class="row">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					  <!-- Indicators -->
					  <ol class="carousel-indicators">
						<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-example-generic" data-slide-to="1"></li>
						<li data-target="#carousel-example-generic" data-slide-to="2"></li>
					  </ol>

					  <!-- Wrapper for slides -->
					  <div class="carousel-inner" role="listbox">
						<div class="item active">
						  <img src="<?php echo get_template_directory_uri(); ?>/img/slider1.jpg" alt="slider">
						  <div class="carousel-caption">
							<h3>sample title</h3>
							<p>this si only for sample description</p>
						  </div>
						</div>
						<div class="item">
						  <img src="<?php echo get_template_directory_uri(); ?>/img/slider2.jpg" alt="slider">
						  <div class="carousel-caption">
							<h3>sample title</h3>
							<p>this si only for sample description</p>
						  </div>
						</div>
					
					  </div>

					  <!-- Controls -->
					  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					  </a>
					  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					  </a>
					</div>
				</div>
			</div>
		</div>
		<div class="service_area">
			<div class="container">
				<div class="row">
				
				</div>
			</div>
		</div>
		<div class="post_area">
			<div class="container">
				<div class="row">
				
				</div>
			</div>
		</div>
		<div class="about_area">
			<div class="container">
				<div class="row">
				
				</div>
			</div>
		</div>
		<div class="footer_area">
			<div class="container">
				<div class="row">
				
				</div>
			</div>
		</div>
		
		
		
		

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
