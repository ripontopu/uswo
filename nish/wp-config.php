<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'usow');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Pm^#VdH{J*S*D&5o9#yj3v1%M8(N{Oj2<?}J[z;]2|IU0eYrucv%JGkw>:~xQyBn');
define('SECURE_AUTH_KEY',  '}Hs$Z^GRRU<>(A^iioEf?_Z2l0|6)&XxrL:;{?z^fi3E_W~NFpuK]&?Xd4P21>NR');
define('LOGGED_IN_KEY',    'BCghq^OxL|`eO,d117EmF^f7t;&x=t(N74zWg@%A7VEQlZq,{],z?1}2dtzjDi)#');
define('NONCE_KEY',        'TZeytg|~(fx+Xk|Y^HvhL}/WQHrvhis!Z-:.[4aXJ[qZ%!GZ<q:/=ft1:!z!0y0~');
define('AUTH_SALT',        'JM+NwBNyP3F!^Of2^OeP+`uzHk%2EXCF4Pnsx>LA@^mh&7)Tsm[_5IB|0[q^4S,|');
define('SECURE_AUTH_SALT', 'R*miCr.-H/o5, 3EO*3#^m{pDnl/zhFca[YXz9?J=`|%t?x) z&hIw0{6s^gyhF-');
define('LOGGED_IN_SALT',   '=xbdl(z%x&%d26>svpARLEGRU-G+g01.mLQXi?+(w.n4Z)tf]{-&FbL|9vrN3ZK#');
define('NONCE_SALT',       'wGNaJ2~@l{Z,yn`6zd7dl5%HD)PMQ vN~K39cHq|9{PiwQ)&`KdX|%6)MZy{(z_O');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'nish_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
